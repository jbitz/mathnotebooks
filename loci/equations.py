#! /bin/python3
from PIL import Image, ImageDraw
import AlgebraicExpressions as AE
import math
import os
from IPython.display import display

IMAGE_WIDTH = 600
IMAGE_HEIGHT = 600
MIN_X = -4
MAX_X = 4
MIN_Y = -4
MAX_Y = 4

EQUALITY_THRESHOLD = .02

MINOR_GRIDLINE_WIDTH = 1
MAJOR_GRIDLINE_WIDTH = 3


#Represents a line defined via slope and y-intercept
class AlgebraicLine:

    ALGEBRAIC_LINE_WIDTH = 3

    # if slope == float('inf'), then y_int is actually the x-int
    def __init__(self, slope, y_int, color):
        self.m = slope
        if slope == 0:
            self.m_perp = float('inf')
        elif slope == float('inf'):
            self.m_perp = 0
        else:
            self.m_perp = -1 / self.m # cache negative reciprocal of slope

        self.b = y_int
        self.projection_matrix = []
        #self.calculate_projection_matrix() # cache projection matrix values
        self.color = color

    def __str__(self):
        return 'Line: y = ' + str(self.m) + '*x+' + str(self.b)

    def __repr__(self):
        return self.__str__()

    # This is actually not super useful as it turns out - the point-slope
    # method implemented below is more useful. Just saving this for posterity
    def calculate_projection_matrix(self):
        """ 
            Calculates the projection matrix corresponding the linear subspace
            parallel to this line. We store the result because it is used in a
            lot of distance calculatations.

            This matrix is given by

            P = [ 1   1   ] [ 1  0 ] [ 1   1   ]^-1
                [ m  -1/m ] [ 0  0 ] [ m  -1/m ]

            (in other words, convert to a basis given by this line and
            its perpendicular, cross out the perpendicular component, then
            convert back to the standard basis).
            The expression below is a reduced form of this product
        """
        m = self.m #slope
        k = (-1 / m) - m #determinant of COB matrix
        self.projection_matrix = [ [(1/k) * (-1/m), -1/k], [-1/k, -m/k] ]

    def closest_point_to(self, point):
        """
            Calculates the point of intersection between this line and the
            line with perpendicular slope going through the given point.

            This line:  y = mx + b
            Other line: y - y_1 = (-1/m) (x - x_1)
        """
        x_1, y_1 = point

        if self.m == 0:
            return (x_1, self.b)
        elif self.m == float('inf'):
            return (self.b, y_1) #b is actually the x-intercept in the vertical case

        closest_x =  (y_1 - self.b - (x_1) * self.m_perp) / (self.m - self.m_perp)
        closest_y = self.m * closest_x + self.b
        return (closest_x, closest_y)

    def squared_distance_to(self, point):
        closest_point = self.closest_point_to(point)
        return (point[0] - closest_point[0]) ** 2 + (point[1] - closest_point[1]) ** 2

    def distance_to(self, point):
        return math.sqrt(self.squared_distance_to(point))

    def draw(self, im, draw_context, curves):
        #Note: Don't need to worry about drawing offscreen - PIL takes care of it
        if self.m != float('inf'):
            y_1 = self.m * MIN_X + self.b
            y_2 = self.m * MAX_X + self.b
            p1 = real_to_screen((MIN_X, y_1))
            p2 = real_to_screen((MAX_X, y_2))
        else: #deal with vertical lines
            p1 = real_to_screen((self.b, MIN_Y))
            p2 = real_to_screen((self.b, MAX_Y))
        draw_context.line([p1, p2], fill=self.color, width = self.ALGEBRAIC_LINE_WIDTH)

class AlgebraicPoint:

    ALGEBRAIC_POINT_RADIUS = 4

    def __init__(self, vals, color):
        self.coords = vals
        self.color = color

    def __str__(self):
        return 'Point: ' + str(self.coords)

    def __repr__(self):
        return self.__str__()

    def squared_distance_to(self, point):
        return (point[0] - self.coords[0]) ** 2 + (point[1] - self.coords[1]) ** 2

    def distance_to(self, point):
        return math.sqrt(self.squared_distance_to(point))

    def draw(self, im, draw_context, curves):
        #bounding box of ellipse
        screen_pos = real_to_screen(self.coords)
        p1 = (screen_pos[0] - self.ALGEBRAIC_POINT_RADIUS, screen_pos[1] - self.ALGEBRAIC_POINT_RADIUS)
        p2 = (screen_pos[0] + self.ALGEBRAIC_POINT_RADIUS, screen_pos[1] + self.ALGEBRAIC_POINT_RADIUS)
        draw_context.ellipse([p1, p2], fill=self.color)

# Represents a curve defined as an algebraic expression in terms of points and lines.
# As an example, if A is a line and B is a point, the curve with the expression
# A = B (i.e. the set of all points equidistant from A and B) is a parabola
class AlgebraicCurve:

    def __init__(self, exp, color):
        self.exp = AE.create_algebraic_expression(exp)
        self.color = color

    def __str__(self):
        return 'Algebraic Curve: ' + str(self.exp)

    def __repr__(self):
        return self.__str__()

    def get_exp(self):
        return self.exp

    def get_color(self):
        return self.color

    def set_color(self, color):
        self.color = color

    def draw(self, im, draw_context, curves):
        for x in range(im.size[0]):
            for y in range(im.size[1]):
                screen_point = (x, y)
                real_point = screen_to_real(screen_point)
                cache = {}
                if self.exp.eval(real_point, curves, cache):
                    draw_context.point(screen_point, fill=self.color)

# ------------------------------------------------------------------------------

def screen_to_real(screen_coords):
    screen_x, screen_y = screen_coords
    real_x = MIN_X + (screen_x / IMAGE_WIDTH) * (MAX_X - MIN_X)
    real_y = MIN_Y + ((IMAGE_HEIGHT - screen_y) / IMAGE_HEIGHT) * (MAX_Y - MIN_Y)
    return (real_x, real_y)

def real_to_screen(real_coords):
    real_x, real_y = real_coords
    screen_x = IMAGE_WIDTH * (real_x - MIN_X) / (MAX_X - MIN_X)
    screen_y = IMAGE_HEIGHT - (IMAGE_HEIGHT * (real_y - MIN_Y) / (MAX_Y - MIN_Y))
    return (screen_x, screen_y)

def draw_grid(im, draw_context):
    #draw x and y axes
    coords = [real_to_screen((0, MIN_Y)), real_to_screen((0, MAX_Y))]
    draw_context.line(coords, width=MAJOR_GRIDLINE_WIDTH, fill='black')
    coords = [real_to_screen((MIN_X, 0)), real_to_screen((MAX_X, 0))]
    draw_context.line(coords, width=MAJOR_GRIDLINE_WIDTH, fill='black')

    #draw vertical lines at every integer coordinate
    for x in range(int(math.ceil(MIN_X)), int(math.floor(MAX_X) + 1)):
        if x == 0: continue
        coords = [real_to_screen((x, MIN_Y)), real_to_screen((x, MAX_Y))]
        draw_context.line(coords, width=MINOR_GRIDLINE_WIDTH, fill='grey')
    for y in range(int(math.ceil(MIN_Y)), int(math.floor(MAX_Y) + 1)):
        if y == 0: continue
        coords = [real_to_screen((MIN_X, y)), real_to_screen((MAX_X, y))]
        draw_context.line(coords, width=MINOR_GRIDLINE_WIDTH, fill='grey')

def get_line():
    m = float(input('m: '))
    b = float(input('b: '))
    color = input('color: ')
    return AlgebraicLine(m, b, color)

def get_point():
    x = float(input('x: '))
    y = float(input('y: '))
    color = input('color: ')
    return AlgebraicPoint((x, y), color)

def get_alg_curve():
    exp = input("Expression in terms of existant (simple) objects: ")
    color = input("color: ")
    return AlgebraicCurve(exp, color)

def delete_object(curves):
    item = input("Object to delete: ")
    curves.pop(item)
    
#returns an Image object to be shown
def draw_objects(curves):
    im = Image.new('RGBA', (IMAGE_WIDTH, IMAGE_HEIGHT), (255,255,255,255))
    draw_context = ImageDraw.Draw(im)
    draw_grid(im, draw_context)

    #draw and show
    for obj in curves:
        curves[obj].draw(im, draw_context, curves)
    return im

def main():
    #Draw a circle via the relation x^2 + y^2 = 1

    im = Image.new('RGBA', (IMAGE_WIDTH, IMAGE_HEIGHT), (255,255,255,255))
    draw_context = ImageDraw.Draw(im)
    draw_grid(im, draw_context)

    curves = {}
    user_eq = input('1) New line 2) New point 3) New algebraic curve 4) Delete object: ')
    current_letter = ord('A')
    while user_eq != '':
        try:
            #add a new object
            choice = int(user_eq)
            if choice == 1:
                new_obj = get_line()
            elif choice == 2:
                new_obj = get_point()
            elif choice == 3:
                new_obj = get_alg_curve()
            elif choice == 4:
                delete_object(curves)

            if choice != 4: #if we didn't just delete something, add it in
                curves[chr(current_letter)] = new_obj
                current_letter += 1

            #recreate image
            im = Image.new('RGBA', (IMAGE_WIDTH, IMAGE_HEIGHT), (255,255,255,255))
            draw_context = ImageDraw.Draw(im)
            draw_grid(im, draw_context)

            #draw and show
            for obj in curves:
                curves[obj].draw(im, draw_context, curves)
            #im.save('out.png')
        except:
            pass
        
        display(im)
        print("Current objects:")
        print(curves)
        user_eq = input('1) New line 2) New point 3) New algebraic curve 4) Delete object: ')

if __name__ == "__main__":
    main()
