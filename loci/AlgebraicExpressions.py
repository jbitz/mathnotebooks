""" 
    AlgebraicExpressions.py
    ------------------------
    Defines an AlgebraicExpression class which can parse and calculate arbitrary
    arithmetic relations involving the distances between algebraically defined
    curves.

    Usage:
    (assume the curve 'A' already is in the dictionary 'curves')

    import AlgebraicExpressions as AE
    my_exp = AE.create_algebraic_expression('(2+A)/(3+3)')
    my_exp.eval((0, 1), curves, {})

    (will return the result of this expression with the distance between the
    point (0, 1) and the curve A substituted in for the variable 'A')

    Note bene: Users should never initialize AlgebraicExpression classes directly
    using the constructor. Instead, they should call the create_algebraic_expression() 
    function, which correctly instantiates the whole tree of relevant sub-expressions.
"""
from enum import Enum

class ExpressionType(Enum):
        CONSTANT = 1 # a constant like '1'
        IDENTIFIER = 2 # an identifier like A
        COMPOUND = 3 # a compound like 2 * A
        RELATION = 4 # a relation like 2 = A or 2 > A (evaluates to T/F)
    
class AlgebraicExpression:
   
    #Allowed operators for relations
    RELATIONAL_OPERATORS = '=<>'

    #exp: a string representation of this expression, i.e. 'A=B'
    def __init__(self, exp, exp_type, op='', left_exp=None, right_exp=None, eq_threshold=.02):
        self.exp_string = exp
        self.exp_type = exp_type

        if self.exp_type == ExpressionType.CONSTANT: #If it's a constant, cache its value
            self.exp_val = float(self.exp_string)
        elif self.exp_type == ExpressionType.COMPOUND or self.exp_type == ExpressionType.RELATION:
            self.op = op
            self.left_exp = left_exp
            self.right_exp = right_exp
            self.eq_threshold = eq_threshold
        elif self.exp_type == ExpressionType.IDENTIFIER:
            pass #do nothing for now, we just want the string

    def __str__(self):
        if self.exp_type == ExpressionType.CONSTANT or self.exp_type == ExpressionType.IDENTIFIER:
            return self.exp_string
        elif self.exp_type == ExpressionType.COMPOUND:
            #parens can be overkill, but better safe than sorry
            return '(' + str(self.left_exp) + self.op + str(self.right_exp) + ')'
        elif self.exp_type == ExpressionType.RELATION:
            return str(self.left_exp) + self.op + str(self.right_exp)

    # left_exp - an AlgebraicExpression
    def set_left(self, left_exp):
        if self.exp_type != ExpressionType.COMPOUND: 
            print("Error: tried to call set_left on a non-compound expression!")
            return
        self.left_exp = left_exp

    # right_exp - an AlgebraicExpression
    def set_right(self, right_exp):
        if self.exp_type != ExpressionType.COMPOUND:
            print("Error: tried to call set_left on a non-compound expression!")
            return
        self.right_exp = right_exp

    # point - an (x, y) coordinate at which to evaluate the expression
    # curves - a dictionary {string: AlgebraicObject} (i.e. points and lines)
    #          which stores all the curves currently plotted
    # cache - a dictionary of cached values for distances between the given
    #          point and an identifier in curves (i.e. if our expression is A*A,
    #          we don't want to compute the distance to A twice
    def eval(self, point, curves, cache={}):
        if self.exp_type == ExpressionType.CONSTANT:
            return self.exp_val
        if self.exp_type == ExpressionType.IDENTIFIER:
            if self.exp_string not in cache:
                cache[self.exp_string] = curves[self.exp_string].distance_to(point)
            return cache[self.exp_string]
        if self.exp_type == ExpressionType.COMPOUND or self.exp_type == ExpressionType.RELATION:
            left_val = self.left_exp.eval(point, curves, cache)
            right_val = self.right_exp.eval(point, curves, cache)
            if self.op == '+':
                return left_val + right_val
            elif self.op == '-':
                return left_val - right_val
            elif self.op == '/':
                return left_val / right_val
            elif self.op == '*':
                return left_val * right_val
            elif self.op == '^':
                return left_val ** right_val
            elif self.op == '=':
                return abs(left_val - right_val) < self.eq_threshold
            elif self.op == '>':
                return left_val > right_val
            elif self.op == '<':
                return left_val < right_val

            print("Error: unknown operator", self.op, "in expression eval")
            return None


# returns the slice index of the next complete sub-expression in the given string
def find_next_subexpression(exp):
    if exp == '': return ''
    if exp[0].isdigit() or exp[0] == '-' or exp[0] == '.': #it's a number
        cur = 0
        while cur < len(exp) and (exp[cur].isdigit() or exp[cur] == '.' or cur == 0 and exp[cur] == '-'):
            cur = cur + 1
        return cur
    if exp[0].isalpha(): #it's an identifier
        return 1
    if exp[0] == '(': #it's parenthesized, we need to search for its partner
        cur = 1
        total = 1
        #count parens until we've totally matched our original guy
        while total != 0 and cur < len(exp):
            if exp[cur] == '(':
                total += 1
            elif exp[cur] == ')':
                total -= 1
            cur += 1
        if total == 0:
            return cur
    
    print("Error: ", exp, " does not have a sub-expression you dunce!")
    return None

#extracts a relational operator from the given expression
def extract_relational_operator(exp):
    for c in AlgebraicExpression.RELATIONAL_OPERATORS:
        if c in exp:
            return c
    return ''

#exp - the original expression, i.e. "A>B"
#rel - the extracted operator, i.e. ">"
#given an expression which we know has a relation in it, represents it as
#an AlgebraicExpression object
def create_relational_expression(exp, rel):
    left_exp = exp[:exp.find(rel)] 
    right_exp = exp[exp.find(rel) + 1:]
    left_rel = extract_relational_operator(left_exp) != ''
    right_rel = extract_relational_operator(right_exp) != ''
    if left_rel or right_rel:
        print("Error: To many relations in expression", exp)
        return None
    left_alg_exp = create_algebraic_expression(left_exp)
    right_alg_exp = create_algebraic_expression(right_exp)
    return AlgebraicExpression(exp, ExpressionType.RELATION, op=rel, left_exp=left_alg_exp, right_exp=right_alg_exp)

# NOTE: At the moment, all operators have equal precedence :( and order of eval
# is left-to-right
def create_algebraic_expression(exp):
    exp = exp.replace(' ', '').upper() #remove unnecessary spaces

    #If it's a relation, we basically split it first and handle seperately
    rel = extract_relational_operator(exp)
    if rel != '':
        return create_relational_expression(exp, rel)

    #Otherwise, handle it normally
    next_exp = exp[:find_next_subexpression(exp)]
    exp = exp[len(next_exp):]

    #Base case - one token
    if exp == '':
        if next_exp == '': return None
        elif next_exp[0].isdigit() or next_exp[0] == '-' or next_exp[0] == '.': #it's a number
            return AlgebraicExpression(next_exp, ExpressionType.CONSTANT)
        elif next_exp[0].isalpha(): #it's an identifier
            return AlgebraicExpression(next_exp, ExpressionType.IDENTIFIER)
        elif next_exp[0] == '(': #it was a pathological single thing in parentheses, i.e. '(A+B)'
            return create_algebraic_expression(next_exp[1:len(next_exp)-1])
        return None

    #recursive case - we need to build it up left to right
    root = create_algebraic_expression(next_exp)
    while exp != '':
        #if we get in here, the next thing should be an operator
        op = exp[0]
        cpd = AlgebraicExpression('', ExpressionType.COMPOUND, op) #we're lazy and don't name it 'cuz that's hard
        exp = exp[1:] #remove that character

        #grab the next token left-to-write and parse it
        next_exp = exp[:find_next_subexpression(exp)]
        exp = exp[len(next_exp):]
        right = create_algebraic_expression(next_exp)

        #rotate the tree so that the prev. root gets a new parent
        cpd.set_left(root)
        cpd.set_right(right)
        root = cpd

    return root



