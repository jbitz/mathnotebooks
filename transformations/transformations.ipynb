{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Creating a Photo Editor: Algebraic Rules for Transformations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "\n",
    "In 2013, the software company Adobe changed its business model, so that users could no longer simply buy their famous Photoshop software. Instead, users now have to pay a monthly subscription fee to access the program - *yuck*! Luckily, with your knowledge of geometric transformations, you can get started writing your own photo editing software! \n",
    "\n",
    "In the past, we've used tables like the one below to reperesent how the points that make up a shape change when we transform them with a translation, reflection, or rotation:\n",
    "\n",
    "| $(x, y)\\mapsto$       | $(x, -y)$ |\n",
    "| ----------- | ----------- |\n",
    "| $(1, 2)$    | $(1, -2)$ |\n",
    "| $(4, -3)$   | $(4, 3)$  |\n",
    "| $(-2, 1)$   | $(-2, -1)$|\n",
    "\n",
    "A 2D image is nothing more than a bunch of points that each have a color - if the center of the image is the origin $(0, 0)$, we can find the coordinates of each point and then use a rule to send it to a new location. This is how we can modify and edit images! We'll explore several real-life image edits in the problems below. \n",
    "\n",
    "## Before Moving On: A Note About JupyterLab\n",
    "If you haven't done any programming before, JupyterLab can seem very intimidating. However, most of the computer code you see here is just to make the problems work - you don't have to change it or even understand it yourself. In this project, you will only neeed to modify the one line that says:\n",
    "```python\n",
    "    return (x, y)\n",
    "```\n",
    "This looks a lot like the rules we've written in class, and we can modify it in the same way. For example, if we wanted to change the rule for our new image to be $(y, -x)$, we would modify this line to say\n",
    "```python\n",
    "    return (y, -x)\n",
    "```\n",
    "\n",
    "Make sure to keep the word ``` return ``` at the start - this is what lets the computer know where your formula is.\n",
    "\n",
    "## Getting Started\n",
    "In order for the mini-programs you'll write below to work, you need to tell JupyterLab how to set itself up. To do this, click anywhere in the big grey box below, and press Shift+Enter. Nothing should happen, but you'll see the little number to the left increase by one - that means it worked. You can then move on to the \"Problems\" section below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from PIL import Image\n",
    "from IPython.display import display\n",
    "\n",
    "def image_to_graph(coords, width, height):\n",
    "    x, y = coords\n",
    "    return (x - width // 2, -1 * (y - height // 2))\n",
    "    \n",
    "def graph_to_image(coords, width, height):\n",
    "    x, y = coords\n",
    "    return (x + width // 2, -y + height // 2)\n",
    "\n",
    "def graph_image(transform, filename=\"obama.jpg\"):\n",
    "    old_im = Image.open(filename)\n",
    "    width, height = old_im.size\n",
    "\n",
    "    new_im = Image.new(mode='RGB', size=old_im.size)\n",
    "    old_pix = old_im.load()\n",
    "    new_pix = new_im.load()\n",
    "\n",
    "    for x in range(width):\n",
    "        for y in range(height):\n",
    "            source_pix = old_pix[x, y]\n",
    "            dest = graph_to_image(transform(image_to_graph((x, y), width, height)), width, height)\n",
    "            if 0 <= dest[0] < width and 0 <= dest[1] < height:\n",
    "                new_pix[dest] = source_pix\n",
    "\n",
    "    display(new_im)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Problems\n",
    "\n",
    "1) **Get JupyterLab to show an image.** To get an image to show up, click on the box below and press Shift+Enter. After a second or so, an image should appear. Who is it? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_1(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "2) **Flipping Out**. Change the formula in the box below to read ```return (x, -y)```, then press Shift+Enter again to create a new image. How has the image changed? Why does this make sense for the rule we just put in?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_2(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "3) **Flipping Out, pt. 2**. Often, selfie cameras contain the option to flip your photo horizontally, so that it doesn't look as if you're staring into a mirror. Change the formula below to flip the image horizontally - what rule did you use?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_3(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "4) **Spin To Win:** In addition to flipping, we might also want to rotate an image. Fill in a formula that rotatates an image *90 degrees counter-clockwise*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_4(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "5) **Sping To Win, pt. 2.** It's also possible that we might take a photo upside-down. Write a formula to rotate an image *180 degrees*. How does the result compare to your image from Problem 2?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_5(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "6) **Explore.** Use the box below to explore other types of formulas that might interest you. For each rule you try, write down a brief description of what happened - you could definitely create something that we haven't talked about yet in class! Some ideas:\n",
    "\n",
    "a) What if $x$ and $y$ appear in both coordinates of the rule - something like $(x+y, y)$ or $(x+y, y-x)$?\n",
    "\n",
    "b) What if we put multipliers (or *coefficients*) in front of the variables, like $(2x, 2y)$? (**Note:** you need to explicitly type out the ```*``` symbol for multiplication when writing your rule, like ```return (2*x, 2*y)```. or else you'll get a nasty error message)\n",
    "\n",
    "c) What rule might get us a 45 degree rotation? (This one is especially challenging)\n",
    "\n",
    "d) Anything else your imagination conjures up!\n",
    "\n",
    "*You can also change the photo to one of your choice - just change the ```obama.jpg``` part in the box below to be the name of any image file shown on the left side of the screen. You can also upload your own by clicking on the \"up arrow\" button at the top left.*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_transform_6(coords):\n",
    "    x, y = coords\n",
    "    #EDIT THIS LINE:####################################\n",
    "    return (x, y)\n",
    "    ####################################################\n",
    "\n",
    "graph_image(my_transform_6, filename='obama.jpg')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
